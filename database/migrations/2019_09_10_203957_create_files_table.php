<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();


            $table->string('extension')->nullable();
            $table->unsignedInteger('size')->nullable();
            $table->text('original_name')->nullable();
            $table->string('uid');
            $table->text('file_signature');
            $table->text('old_file_signature')->nullable();
            $table->text('fullpath');
            $table->text('relative_path');
            $table->text('mimetype')->nullable();
            $table->text('saved_name');

            $table->boolean('is_active')->default(true);

            $table->boolean('is_anon')->default(true);
            $table->unsignedInteger('views')->default(1);
            $table->unsignedInteger('downloads')->default(1);



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
