<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('site_title')->nullable();
            $table->string('site_logo_text')->default('FileBucket');

            $table->text('site_description')->nullable();

            $table->boolean('is_theme_switch_enabled')->default(true);
            $table->text('default_theme')->default('light');


            $table->boolean('disable_current_uploads')->default(false);
            $table->boolean('disable_share_buttons')->default(false);
            $table->boolean('disable_recent_uploads')->default(false);
            $table->boolean('disable_popular_uploads')->default(false);
            $table->boolean('disable_api')->default(false);


            // file settings

            $table->bigInteger('file_maxsize_kb')->default('150000');
            $table->bigInteger('file_minsize_kb')->default('10');






            $table->unsignedInteger('max_file_size')->default(10000);

            $table->text('css_after')->nullable();
            $table->text('js_after')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
