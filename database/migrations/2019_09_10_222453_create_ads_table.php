<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->boolean('is_header_ad')->default(false);
            $table->boolean('is_left_sidebar_ad')->default(false);
            $table->boolean('is_right_sidebar_ad')->default(false);
            $table->boolean('is_bottom_ad')->default(false);

            $table->text('header_ad')->nullable();
            $table->text('left_sidebar_ad')->nullable();
            $table->text('right_sidebar_ad')->nullable();
            $table->text('bottom_ad')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ads');
    }
}
