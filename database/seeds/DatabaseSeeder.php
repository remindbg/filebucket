<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(AdsTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(StaticPagesTableSeeder::class);
        $this->call(UserTableSeeder::class);

    }
}
