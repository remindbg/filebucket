<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([

            'site_title'             => 'FileBucket Script',
            'site_description'       => 'Default Site Description',
            'is_theme_switch_enabled'          =>true,

            'default_theme'          => 'light',
            'disable_current_uploads' => false,
            'css_after'              => '',
            'js_after'               => '',




            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

    }
}
