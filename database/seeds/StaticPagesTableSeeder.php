<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StaticPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('static_pages')->insert([
            'homepage'              => 'Homepage Text',
            'contact'               => 'Contact Page text',
            'rules'                 => 'Rules Page text',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);

    }
}
