<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->insert([
            'is_header_ad' => false,
            'is_left_sidebar_ad' => false,
            'is_right_sidebar_ad' => false,
            'is_bottom_ad' => false,


            'header_ad' => '',
            'left_sidebar_ad' => '',
            'right_sidebar_ad' => '',
            'bottom_ad' => '',

            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

    }
}
