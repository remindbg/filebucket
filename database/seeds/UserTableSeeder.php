<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        DB::table('users')->insert([


            'email_verified_at'          => now(),
            'name'                       => 'admin',
            'password'                   => bcrypt('adminpassword'), // password
            'remember_token'             => Str::random(10),
            'email'                      => 'admin@admin.com',
            'is_demo'                    => 0,
            'is_admin'                   => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);
        DB::table('users')->insert([

            'email_verified_at'          => now(),
            'password'                   => bcrypt('demo'), // password
            'remember_token'             => Str::random(10),
            'name'                       => 'demo',
            'email'                      => 'demo@oniwp.com',
            'is_demo'                    => 1,
            'is_admin'                   => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

    }
}
