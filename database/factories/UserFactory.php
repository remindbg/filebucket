<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('password'), // password
        'remember_token' => Str::random(10),
        'is_banned' => $faker->boolean(5),
        'is_profile_private' => $faker->boolean(10),
        'is_upload_allowed' => $faker->boolean(90),
        'about' => $faker->paragraph,
        'closed_for' => $faker->paragraph,
        'is_demo' => $faker->boolean(3),

        'role_id' => $faker->randomElement($array = array (1,2,3)),

    ];
});
