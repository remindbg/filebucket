<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\User;
use App\File;
$factory->define(File::class, function (Faker $faker) {

    $extension = ['php','js','exe','docx','html'];

        return [
            'name' => $faker->streetAddress,
            'size' => $faker->numberBetween(5,300000),
            'uid' => Str::random(15),
            'description' => $faker->text(200),
            'extension' => $faker->randomElement($extension),
            'is_removed' => $faker->boolean($chanceOfGettingTrue = 5),
            'is_removed_by_admin' => $faker->boolean($chanceOfGettingTrue = 5),
            'is_anon' =>$faker->boolean($chanceOfGettingTrue = 50),
            'is_private' => $faker->boolean($chanceOfGettingTrue = 10),
            'is_unlisted' => $faker->boolean($chanceOfGettingTrue = 10),
            'are_comments_enabled' => $faker->boolean(90),
            'is_admin_approved' => $faker->boolean(90),
            'views' => $faker->numberBetween(1,10000),

            'user_id' => User::all()->random()->id
        ];

});
