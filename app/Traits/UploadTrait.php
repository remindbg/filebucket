
namespace App\Traits;

use Illuminate\Http\UploadedFile;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'public', $filename = null)
    {
        $data = [];
        $name = !is_null($filename) ? $filename : str_random(15);

        $file = $uploadedFile->storeAs($folder, $name . '.' . $uploadedFile->getClientOriginalExtension(), $disk);
        $data['filepath'] = $file;
        $data['filename'] = $name . $uploadedFile->getClientOriginalExtension();

        return $data;

    }
}
