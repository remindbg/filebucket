<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $guarded = [];
    // since we have an admin middleware, we dont care about mass assignment
}
