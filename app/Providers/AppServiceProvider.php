<?php

namespace App\Providers;

use App\File;
use App\Settings;
use App\User;
use Barryvdh\Debugbar\DataCollector\FilesCollector;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Ad;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        view()->composer(['*'], function
        ($view) {
            $frontsettings = Settings::find(1);
            $frontads = Ad::find(1);



            $view->with(['frontsettings' => $frontsettings ,
                            'frontads' => $frontads

            ])
            ;
        });



        view()->composer(['admin._static.sidebar'], function
        ($view) {

           $userscount = User::all()->count();
           $filescount = File::all()->count();




            $view->with([
                    'userscount' => $userscount ,
                    'filescount'      => $filescount

            ])
            ;
        });

    }
}
