<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use App\Settings;

class UploadController extends Controller
{
    private $anonpath;

    public function __construct()
    {
        $this->anonpath = '/uploads/anonuploads';

    }



    public function store(Request $request)
    {


        $errors = [];
        $settings = Settings::find(1);

        $path = $this->anonpath;

        $files = $request->file('file');

        if (!is_array($files)) {
            $files = [$files];
        }

           if (!is_dir(public_path($path))) {
                mkdir(public_path($path), 0777);
           }

        for ($i = 0; $i < count($files); $i++) {
            $file = $files[$i];

            $name = sha1(date('YmdHis') . str_random(8));
            $save_name = $name . '.' . $file->getClientOriginalExtension();
            $filepath = $path . '/'. $save_name;




            // check for the file size

            if ($this->bytesToHuman($settings->file_maxsize_kb)    >=  $this->bytesToHuman($file->getSize()))
                {
                    Log::info('TEST TOO BIG ' . $this->bytesToHuman($file->getSize()));

                    return response()->json([

                        'error' =>'413',
                        'message' => 'File is Too Big - Skipped uploading',
                        'file' => 'testttt',
                        'file_url' => 'test',
                        'file_slug' => 'testtttttt',

                    ], 200);
                }

            elseif($this->bytesToHuman($settings->file_minsize_kb) > $this->bytesToHuman($file->getSize()))
            {
                return response()->json([

                    'error' =>'413',
                    'message' => 'File is too small - Skipped uploading',
                    'file' => 'testttt',
                    'file_url' => 'test',
                    'file_slug' => 'testtttttt',

                ], 200);
            }

            else {  // file is valid
// assume file is legit, attempt to save it
                $filedb = new File();
                $filedb->extension = $file->getClientOriginalExtension();
                $filedb->size = $file->getSize();




                Log::info('size is: ' . $this->bytesToHuman($filedb->size));
                $filedb->original_name = $file->getClientOriginalName();

                // make a better file name



                $filedb->uid = strtolower(Str::random(7));
                $filedb->file_signature = hash('sha256',$name);

                $filedb->fullpath = URL::to($path . '/' . $save_name);
                $filedb->saved_name = $save_name;
                $filedb->relative_path = $filepath;
                $filedb->mimetype = $file->getClientMimeType();

                $filedb->is_anon = true;


                $filedb->save();
                $file->move(public_path($path), $save_name);

                $slugpath = 'download/' . $filedb->id . '/'. $filedb->uid;

                $fileslug = URL::to($slugpath);

                return response()->json([

                    'response' =>'200',
                    'message' => 'Image saved Successfully',
                    'file' => $file,
                    'file_url' => $fileslug,
                    'file_slug' => $fileslug,

                ], 200);
            }


        }
    }

    public   function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2);
    }


}
