<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('home');
    }

    public function testmessages() {
        return redirect()->route('home.index')
            ->with('info','test info component flash message')
            ->with('success','test success component flash message')
            ->with('warning','test warning component flash message')
            ->with('error','test danger component flash message')
            ;
    }
}
