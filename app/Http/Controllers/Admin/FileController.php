<?php

namespace App\Http\Controllers\Admin;

use App\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FileRequest;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = File::with('user')->orderBy('created_at','DESC')->paginate(10);

        return view('admin.files.index',compact('files'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FileRequest $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $file = File::find($id);

        if($file) {
            return view('admin.files.edit',compact('file'));

        }
        else {
            return back()->with('error','File Not Exist');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = File::find($id);
        $data = $request->all();

        if($file){
            $file->update($data);

            return redirect()->route('admin.files.index')->with('success','File is Edited');

        }
        else {
            return redirect()->back()->with('error','File with the ID not found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = File::find($id);
        $path =   public_path($file->relative_path);
        $relpath = $file->fullpath;
        $filemessage = 'And file not removed from DISK';


        if(@unlink($path)) {
                $filemessage = 'And file removed from disk';

            }



        $file->delete();

        return redirect()->route('admin.files.index')->with('success','File removed from DB ' . $filemessage);

    }
}
