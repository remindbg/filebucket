<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
class ThemeController extends Controller
{

    public function switchTheme($theme) {

        switch ($theme) {
            case 'light':


                Cookie::queue('theme', 'light',time() + 60 * 60 * 24 * 365);
                break;


            case 'night':
                Cookie::queue('theme', 'night',time() + 60 * 60 * 24 * 365);

                break;

            default:
                break;
        }


        return back()->with('info','Theme Changed');

    }
}
