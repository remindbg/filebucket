<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use App\Settings;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class FileController extends Controller
{


  public function getMetaFileSettings() {
   $settings = Settings::find(1);
   $filesettings = [];
   $filesettings->maxsize = $settings->file_maxsize_kb;
   $filesettings->minsize = $settings->file_minsize_kb;


    return response()->json($filesettings,200);


  }

    public function popular() {
        $files = File::
            where('is_active',1)
            ->orderBy('views','DESC')
            ->paginate(5);

        if($files) {
            return view('files.popularfiles',compact('files'));
        }
    }

    public function latest()
    {
        $files = File::with('user')
            ->where('is_active',1)
            ->orderBy('created_at','DESC')
            ->paginate(5);

        if($files) {
            return view('files.recentfiles',compact('files'));
        }

    }



    public function single($id, $fileslug)
    {
        $file = File::find($id);

        if($file) {
            $file->views ++;
            $file->save();

            return view('files.single',compact('file'));


        }
        else {
            redirect()->back()->with('error','File Not Found');

        }

    }


}
