<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class File extends Model
{
    protected $guarded = [];


    public function user() {
        return $this->belongsTo('App\User');
    }
    public function fileslug() {
        return URL::to('download/'. $this->id . '/' . $this->uid);
    }

    public function imgsrc() {
        return $this->fullpath;
    }

    public function is_image() {
        $imageExtensions = ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'svg',
            'svgz', 'cgm', 'djv', 'djvu', 'ico', 'ief','jpe', 'pbm', 'pgm', 'pnm',
            'ppm', 'ras', 'rgb', 'tif', 'tiff', 'wbmp', 'xbm', 'xpm', 'xwd'];

        $explodeImage = explode('.', 'image-name-here');
        $extension = $this->extension;

        if(in_array($extension, $imageExtensions))
        {
            return true;
        }
            else
        {
            return false;
        }
    }



}
