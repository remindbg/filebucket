/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require('./dropzone');


Dropzone.autoDiscover = false;







// Get the template HTML and remove it from the document
// var previewNode = document.querySelector("#template");
// previewNode.id = "";
// var previewTemplate = previewNode.parentNode.innerHTML;
// previewNode.parentNode.removeChild(previewNode);
var previewTemplate = '<div id="template" class="file-row"><div><div class="preview"><img data-dz-thumbnail class="img-fluid rounded thumbnail" /><i class="fas fa-file-alt file-icon d-none"></i></div></div><div><p class="size mb-2" data-dz-size></p><div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div></div></div><div><p class="name" data-dz-name></p><!-- <input class="form-control d-none file-url"> --><div class="input-group d-none file-url"><input type="text" class="form-control file-url-input" placeholder="File URL" aria-label="File URL" aria-describedby="button-addon2" data-placement="top" data-toggle="tooltip" title="Copied!"><div class="input-group-append"><button class="btn btn-dark" type="button" id="button-copy-url"><i class="far fa-copy"></i> <span>Copy</span></button></div></div><span class="error text-danger" data-dz-errormessage></span></div><div class="buttons text-right"><button class="btn btn-outline-dark start"><i class="fas fa-cloud-upload-alt"></i> <span>Upload</span></button> <button data-dz-remove class="btn btn-outline-secondary cancel"><i class="fas fa-ban"></i> <span>Cancel</span></button><a class="btn btn-success view d-none" target="_blank"><i class="far fa-eye"></i> <span>View</span></a><!-- <button data-dz-remove class="btn btn-danger delete"><span>Delete</span></button> --></div></div>';

var fBucketDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: uploadRoute, // Set the url
    method: 'POST',
    headers: {
        'x-csrf-token': document.querySelectorAll('meta[name=csrf-token]')[0].getAttributeNode('content').value,
    },
    thumbnailWidth: 96,
    thumbnailHeight: 96,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button", // Define the element that should be used as click trigger to select files.
    paramName: "file",
    maxFilesize: 2000, // MB
    maxFiles: 3,
    // renameFile: function(file) {
    //     let name_base = file.name.replace(/\.[^/.]+$/, ""); // file name without the extension
    //     let name_length = name_base.length;
    //     let new_name = file.name;
    //     if (name_length > 10) {
    //         let fileExt = getFileExtension(file.name);
    //         new_name = name_base.substring(0, 8) + '_.' + fileExt;

    //     }
    //     console.log('new_name = ' + new_name);
    //     return new_name;
    // }
});

let start_button = document.querySelector('#actions .start');
let cancel_button = document.querySelector('#actions .cancel');
let lead = document.querySelector('.lead');
let upload_all_btn = document.querySelector('#actions .start');
let content = document.querySelector('.content');

fBucketDropzone.on("addedfile", function(file) {
    // Hookup the start button
    let file_start_button = file.previewElement.querySelector(".start")
    file_start_button.onclick = function() { fBucketDropzone.enqueueFile(file); };
    let file_options = document.querySelector('.file-options');
    removeClass(file_options, 'invisible');
    // document.querySelector(".cancel").style.opacity = "1";
    let fileExt = getFileExtension(file.name);
    let imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];
    if (!imageExtensions.includes(fileExt)) {
        let thumbnail = file.previewElement.querySelector(".thumbnail");
        let fileIcon = file.previewElement.querySelector('.file-icon');
        addClass(thumbnail, 'd-none');
        removeClass(fileIcon, 'd-none');
    }

    removeClass(start_button, 'd-none');
    removeClass(cancel_button, 'd-none');

    lead.style['font-size'] = '1.5rem';
    content.style['padding-bottom'] = 0;
});

// Update the total progress bar
fBucketDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

fBucketDropzone.on("sending", function(file, xhr, formData) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1";
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    // Send the 'public' option as well
    // let is_public = document.querySelector('#is-public').value;
    // formData.append("is_public", is_public);
    // console.log('file.upload.filename = ' + file.upload.filename);
});

// Hide the total progress bar when nothing's uploading anymore
fBucketDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0";
    addClass(upload_all_btn, 'd-none');
});

// Setup the buttons for all transfers
// The "add files" button doesn't need to be setup because the config
// `clickable` has already been specified.
start_button.onclick = function() {
    fBucketDropzone.enqueueFiles(fBucketDropzone.getFilesWithStatus(Dropzone.ADDED));
};

cancel_button.onclick = function() {
    fBucketDropzone.removeAllFiles(true);
    addClass(start_button, 'd-none');
    addClass(cancel_button, 'd-none');
    lead.style['font-size'] = '1.75rem';
};

// fBucketDropzone.on("removedfile", function(file) {
//     // Hookup the start button
//     console.log(file);
// });

fBucketDropzone.on("success", function(file, response) {

    // invalid response from the server

    if(response.error) {
        console.log(response.message);


    }
    else {
        let file_size = file.previewElement.querySelector('p.size');
        // console.log(file);
        // console.log(response);
        let file_name = file.previewElement.querySelector('p.name');
        addClass(file_name, 'd-none');
        let file_url = file.previewElement.querySelector('.file-url');
        removeClass(file_url, 'd-none');
        let file_url_input = file.previewElement.querySelector('.file-url-input');
        file_url_input.value = response.file_url;

        // let delete_button = file.previewElement.querySelector('.btn.delete')
        // addClass(delete_button, 'd-none');

        // let progressbar_all = document.querySelector('.fileupload-process');

        let cancel_button = document.querySelector('.btn.cancel');
        cancel_button.innerHTML = '<i class="fas fa-ban"></i> <span>Clear</span>';
        cancel_button.addEventListener('click', function() {
            cancel_button.innerHTML = '<i class="fas fa-ban"></i> <span>Cancel</span>';
            document.querySelector("#total-progress").style.opacity = "0";
            let file_options = document.querySelector('.file-options');
            addClass(file_options, 'invisible');
        })

        let btn = file.previewElement.querySelector('#button-copy-url');

        btn.addEventListener('click', function(event) {
            event.preventDefault();
            $('.file-url-input').tooltip();
            copyToClipboard(file_url_input);

        });

        let btn_view = file.previewElement.querySelector('.btn.view');
        btn_view.href = response.file_url;
        removeClass(btn_view, 'd-none');

        // div.appendChild(input);
        // div.appendChild(btn);
        // addClass(div, 'dz-filename');
        file.previewElement.querySelector(".input-group-append").appendChild(btn);

        file_size.style["padding-top"] = '21px';
    }



});

fBucketDropzone.on("error", function(file, errorMessage) {
    let error_el = file.previewElement.querySelector('.error');
     error_el.textContent = "Error uploading file!";
    error_el.textContent = errorMessage;

});

function copyToClipboard(el) {
    el.select();
    el.setSelectionRange(0, 99999); /*For mobile devices*/
    document.execCommand("copy");
}

function getFileExtension(filename) {
    return filename.split('.').pop();
}



// Night/Day mode

let nightLink = document.querySelector('.daynight.night');
let dayLink = document.querySelector('.daynight.day');

nightLink.addEventListener('click', function() {
    event.preventDefault();
    removeClass(document.body, 'day');
    addClass(document.body, 'night');
    addClass(nightLink, 'd-none');
    removeClass(dayLink, 'd-none');
});

dayLink.addEventListener('click', function() {
    event.preventDefault();
    removeClass(document.body, 'night');
    addClass(dayLink, 'd-none');
    removeClass(nightLink, 'd-none');
});

function addClass(el, className) {
    if (el.classList) el.classList.add(className);
    else if (!hasClass(el, className)) el.className += ' ' + className;
}

function removeClass(el, className) {
    if (el.classList) el.classList.remove(className);
    else el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
}
