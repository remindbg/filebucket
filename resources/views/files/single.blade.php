@extends('layouts.app')

@section('css')

@endsection


@section('meta')
    <title> Free download of {{$file->is_image() ? 'image: ' : 'file: '}}{{$file->original_name}}</title>
    <meta name="description" content="Free download of {{$file->is_image() ? 'image: ' : 'file:
    '}}{{$file->original_name}}.{{$file->extension}} ">

    <meta property="og:title" content=" Free download of {{$file->is_image() ? 'image: ' : 'file: '}}{{$file->original_name}}" />
    <meta property="og:url" content="{{request()->url()}}" />
    <meta property="og:description" content=" Free download of {{$file->is_image() ? 'image: ' : 'file: '}}{{$file->original_name}}" />


@endsection

@section('content')

<div class="row">
    <div class="col">
        <p class="lead mt-1 mb-4">
            <!-- <p><i class="fas fa-cloud-upload-alt"></i></p> -->
            Free download of <strong>{{$file->original_name}}</strong>
        </p>

      <div class="header-ad">
          @if ($frontads->is_header_ad)
              {!! $frontads->header_ad !!}
          @endif

              <hr>
      </div>
    </div>
</div>

<div class="row">

    <div class="col-3">
        @if ($frontads->is_left_sidebar_ad)
            <div class="left-sidebar-ads">
                {!! $frontads->left_sidebar_ad !!}
            </div>

        @endif

    </div>

    <div class="col-6">
        <p>
        @if ($file->is_image())
            <img src="{{ $file->imgsrc() }}" class="img-fluid">
        @else
            <i class="fas fa-file-alt file-icon"></i>
        @endif
        </p>

        <form action="{{route('download.single',['id' => $file->id, 'uid' => $file->uid])}}" method="POST">
            @csrf
            <div class="row file-stats mb-4">
                <p class="col-6 text-left">Views: <span class="badge badge-pill badge-light">{{$file->views}}</span></p>
                <p class="col-6 text-right">Downloads: <span class="badge badge-pill
                badge-light">{{$file->downloads}}</span></p>
            </div>
            <div class="form-group row file-details pb-3 mb-4">
                <label class="control-label col-sm-2 col-form-label signature-label" for="inputSuccess"><!-- <i class="fas fa-check text-success"></i> -->
                    Signature: </label>
                <div class="col-sm-7">
                    <input type="text" class="form-control is-valid disabled signature-input" id="inputSuccess"
                       value="{{$file->file_signature}}" disabled>
                </div>
                <p class="mt-2 col-sm-3 filetype">{{$file->mimetype}}</p>
            </div>

            @if ($frontsettings->disable_share_buttons)

            @else
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_viber"></a>
                    <a class="a2a_button_whatsapp"></a>

                    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                </div>
            @endif

            <hr>
            <div class="row">
                <!-- <p class="col-4 text-left">Views: 23</p> -->
                <div class="col text-center">
                    <div class="bottom-ad">
                        @if ($frontads->is_bottom_ad)
                            {!! $frontads->bottom_ad !!}
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary btn"><i class="fa fa-download
                    mr-2"></i>Download</button>
                </div>

            </div>
        </form>


    </div>


    <div class="col-3">
        <div class="col-3">
            @if ($frontads->is_right_sidebar_ad)
                <div class="right-sidebar-ads">
                    {!! $frontads->left_sidebar_ad !!}
                </div>

            @endif

        </div>

    </div>

</div>


@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')

        <script async src="https://static.addtoany.com/menu/page.js"></script>


@endsection

