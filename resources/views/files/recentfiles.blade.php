@extends('layouts.app')

@section('css_after')

@endsection

@section('content')

<div class="row">
    <div class="col">
        <h1 class="lead mt-1 mb-4">
            <!-- <p><i class="fas fa-cloud-upload-alt"></i></p> -->
            Latest uploaded files
        </h1>
        <p class="mb-4"><a href=""><img src="{{asset('img/digitalocean.png')}}" class="img-fluid"></a></p>
    </div>
</div>

<div class="row">

    <div class="col">

        <!-- <table class="table table-hover">
            <thead>
            <tr>

                <th>Name</th>
                <th>Type</th>
                <th>Date</th>
                <th>Views</th>
                <th>Uploader</th>
                <th>Downloads</th>
                <th>Buttons</th>
            </tr>
            </thead>
            <tbody>
            @forelse($files as $file)
                <tr>
                    <td>{!! \Illuminate\Support\Str::limit($file->original_name, 12,'....')  !!}
                    </td>
                    <td>{{$file->mimetype}}</td>
                    <td>{{$file->created_at->diffForHumans()}}</td>
                    <td>{{$file->views}}</td>
                    <td>
                        @if ($file->user)
                        <td>{!! \Illuminate\Support\Str::limit($file->user->name, 7,'....')  !!}

                        @else
                            Anonymous
                        @endif

                    </td>
                    <td>TODO</td>
                    <td><a href="{{route('files.single',['id' => $file->id,'uid' => $file->uid])}}" class="btn
                    btn-primary btn-sm">View File</a></td>
                </tr>

            @empty

            @endforelse

            </tbody>
        </table> -->


        <div class="card-columns">
            @forelse($files as $file)

                <a href="{{route('files.single',['id' => $file->id,'uid' => $file->uid])}}" class="card">
                    
                    @if ($file->is_image())
                        <img src="{{ $file->imgsrc() }}" class="card-img-top" alt="">
                    @else
                        <i class="fas fa-file-alt file-icon"></i>
                    @endif
                    <div class="card-body">
                        <h5 class="card-title">{!! \Illuminate\Support\Str::limit($file->original_name, 12,'....')  !!}</h5>
                        @if (!$file->is_image())
                            <p class="card-text"><small class="text-muted">{{$file->mimetype}}</small></p>
                        @endif
                        <!-- <p class="card-text"> -->
                            <!-- <button class="btn btn-primary btn-sm position-absolute">Details</button> -->
                            <i class="fas fa-external-link-alt position-absolute link-icon"></i>
                        <!-- </p> -->
                    </div>

                    <!-- <span class="position-absolute badge-views"> {{$file->views}}</span> -->
                    <!-- <span class="badge badge-secondary position-absolute badge-downloads">Downloads: {{$file->downloads}}</span> -->
                    <!-- <button type="button" class="btn btn-sm btn-dark position-absolute badge-views">
                        <i class="far fa-eye"></i> <span class="badge badge-light">{{$file->views}}</span>
                    </button> -->
                    
                </a>
            
            @empty
            @endforelse
        </div>

        <p class="mt-5">{{$files->links()}}</p>


    </div>

</div>


@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')
    <script>
        // var elem = document.querySelector('.grid');
        // var msnry = new Masonry( elem, {
        //     // options
        //     itemSelector: '.grid-item',
        //     columnWidth: 360
        // });
    </script>
@endsection
