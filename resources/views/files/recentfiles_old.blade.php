@extends('layouts.app')

@section('css_after')

@endsection

@section('content')

<div class="row">
    <div class="col">
        <h1 class="lead mt-1 mb-4">
            <!-- <p><i class="fas fa-cloud-upload-alt"></i></p> -->
            Latest uploaded files
        </h1>
        <p class="mb-4"><a href=""><img src="{{asset('img/digitalocean.png')}}" class="img-fluid"></a></p>
    </div>
</div>

<div class="row">

    <div class="col">

        <table class="table table-hover">
            <thead>
            <tr>

                <th>Name</th>
                <th>Type</th>
                <th>Date</th>
                <th>Views</th>
                <th>Uploader</th>
                <th>Downloads</th>
                <th>Buttons</th>
            </tr>
            </thead>
            <tbody>
            @forelse($files as $file)
                <tr>
                    <td>{!! \Illuminate\Support\Str::limit($file->original_name, 12,'....')  !!}
                    </td>
                    <td>{{$file->mimetype}}</td>
                    <td>{{$file->created_at->diffForHumans()}}</td>
                    <td>{{$file->views}}</td>
                    <td>
                        @if ($file->user)
                        <td>{!! \Illuminate\Support\Str::limit($file->user->name, 7,'....')  !!}

                        @else
                            Anonymous
                        @endif

                    </td>
                    <td>TODO</td>
                    <td><a href="{{route('files.single',['id' => $file->id,'uid' => $file->uid])}}" class="btn
                    btn-primary btn-sm">View File</a></td>
                </tr>

            @empty

            @endforelse





            </tbody>
        </table>
        <p>{{$files->links()}}</p>







    </div>

</div>







@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')
    <script>

    </script>
@endsection
