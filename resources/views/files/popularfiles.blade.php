@extends('layouts.app')

@section('css_after')

@endsection

@section('content')


<div class="row">
    <div class="col">
        <h1 class="lead mt-1 mb-4">
            <!-- <p><i class="fas fa-cloud-upload-alt"></i></p> -->
            Popular files
        </h1>
        <p class="mb-4"><a href=""><img src="{{asset('img/digitalocean.png')}}" class="img-fluid"></a></p>
    </div>
</div>

<div class="row">

    <div class="col">


    <div class="card-columns">
        @forelse($files as $file)

            <a href="{{route('files.single',['id' => $file->id,'uid' => $file->uid])}}" class="card">

                @if ($file->is_image())
                    <img src="{{ $file->imgsrc() }}" class="card-img-top" alt="">
                @else
                    <i class="fas fa-file-alt file-icon"></i>
                @endif
                <div class="card-body">
                    <h5 class="card-title">{!! \Illuminate\Support\Str::limit($file->original_name, 12,'....')  !!}</h5>
                    @if (!$file->is_image())
                        <p class="card-text"><small class="text-muted">{{$file->mimetype}}</small></p>
                    @endif
                    <!-- <p class="card-text"> -->
                        <!-- <button class="btn btn-primary btn-sm position-absolute">Details</button> -->
                        <i class="fas fa-external-link-alt position-absolute link-icon"></i>
                    <!-- </p> -->
                </div>

                <!-- <span class="position-absolute badge-views"> {{$file->views}}</span> -->
                <!-- <span class="badge badge-secondary position-absolute badge-downloads">Downloads: {{$file->downloads}}</span> -->
                <!-- <button type="button" class="btn btn-sm btn-dark position-absolute badge-views">
                    <i class="far fa-eye"></i> <span class="badge badge-light">{{$file->views}}</span>
                </button> -->

            </a>

        @empty
        @endforelse
    </div>

        <p>{{$files->links()}}</p>


    </div>


@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('scripts')
    <script>

    </script>
@endsection
