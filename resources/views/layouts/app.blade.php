<!doctype html>
<html lang="en">
@include('_static.head')
<body class="@yield('body')
    @if ($frontsettings->is_theme_switch_enabled)
{{Cookie::get('theme')  == 'day' ? 'day' : 'night'}}
    @else
    {{$frontsettings->default_theme}}

@endif
    ">
<div class="container-fluid">

    @include('components.header')

        <section class="content text-center">

            @include('components.messages')

                <div class="container mb-5">

                       @yield('content')
                </div>

        </section>

@include('_static.footer')

</div>
