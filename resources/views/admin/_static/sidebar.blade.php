<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->


    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">

            <div class="info">

                <a href="#" class="d-block">Welcome</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="{{route('admin.dashboard.index')}}" class="nav-link active">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Welcome
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Site Settings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.settings.index')}}" class="nav-link">
                                <i class="fas fa-cog nav-icon"></i>
                                <p>General Settings</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.siteinfo.index')}}" class="nav-link">
                                <i class="fas fa-info nav-icon"></i>
                                <p>Current Site Information</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.cleaner.index')}}" class="nav-link">
                                <i class="fa fa-trash nav-icon"></i>
                                <p>Dir Cleaner</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/phpinfo" class="nav-link">
                                <i class="fa fa-info nav-icon"></i>
                                <p>Full PHP Info</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Files
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">{{$filescount}}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('admin.files.index')}}" class="nav-link">
                                <i class="fas fa-file-archive nav-icon"></i>
                                <p>All Files</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('admin.filesettings.index')}}" class="nav-link">
                                <i class="fas fa-cog nav-icon"></i>
                                <p>File Settings</p>
                            </a>
                        </li>




                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-secret"></i>
                        <p>
                          Users
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">{{$userscount}}</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.users.index')}}" class="nav-link">
                                <i class="fas fa-user-alt nav-icon"></i>
                                <p>All Users</p>
                            </a>
                        </li>


                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-ad"></i>
                        <p>
                           Ads Management
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.ads.index')}}" class="nav-link">
                                <i class="fas fa-cog nav-icon"></i>
                                <p>Ads</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-pager"></i>
                        <p>
                           Pages
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.staticpages.index')}}" class="nav-link">
                                <i class="fas fa-pager nav-icon"></i>
                                <p>Pages</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Documentation</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
