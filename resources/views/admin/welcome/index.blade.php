@extends('admin.layouts.app')

@section('title','Welcome To Admin Panel')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <h5 class="mb-2">Recent Details</h5>
            <div class="row">


                <div class="col-md-12 col-sm-12 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning"><i class="far fa-copy"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text"><a href="{{route('admin.files.index')}}">View All
                                    Files</a></span>
                            <span class="info-box-number">{{$filescount}}</span>
                        </div>

                    </div>
                    <div class="card card-success">
                        <div class="card-header">
                            <h3 class="card-title">Recent Files</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" style="display: block;">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>file name</th>
                                    <th>is active</th>
                                    <th>Date</th>
                                    <th style="width: 40px">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($latestfiles->sortByDesc('created_at') as $file)
                                    <tr>
                                        <td>{{$file->id}}</td>
                                        <td>{{str_limit($file->original_name),10,'...'}}</td>
                                        <td>
                                            {{$file->is_active ? 'Yes' : 'No'}}
                                        </td>
                                        <td>{{$file->created_at->diffForHumans()}}</td>
                                        <td>
                                            <a href="{{route('admin.files.edit',$file->id)}}" class="btn btn-info
                                            btn-sm">
                                                <i class="fa fa-pencil"></i>Edit
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    No Files Found
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
