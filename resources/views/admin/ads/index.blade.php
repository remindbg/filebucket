@extends('admin.layouts.app')

@section('title','Edit Ads')

@section('content')


    <div class="card-body">
        <form action="{{route('admin.ads.update',1)}}" method="POST"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    @method('put')

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Top Header Ad</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-lg-7">
                                    <div class="input-group mb-3">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="header_ad"> Code <span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="3" name="header_ad"
                                                          id="header_ad">{!! $ad->header_ad !!}</textarea>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Active?</label>
                                        <select class="form-control" name="is_header_ad">
                                            <option value="1" {{$ad->is_header_ad ? 'selected' : ''}}>Yes</option>
                                            <option value="0" {{$ad->is_header_ad ? '' : 'selected'}}>No</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="card card-success ">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Left Sidebar Ad</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-lg-7">
                                    <div class="input-group mb-3">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for=""> Code <span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="3" name="left_sidebar_ad"
                                                >{!! $ad->left_sidebar_ad !!}</textarea>
                                            </div>


                                        </div>


                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Active?</label>
                                        <select class="form-control" name="is_left_sidebar_ad">
                                            <option value="1" {{$ad->is_left_sidebar_ad ? 'selected' : ''}}>Yes</option>
                                            <option value="0" {{$ad->is_left_sidebar_ad ? '' : 'selected'}}>No</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="card card-success ">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Right Sidebar Ad</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-lg-7">
                                    <div class="input-group mb-3">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for=""> Code <span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="3" name="right_sidebar_ad"
                                                >{!! $ad->right_sidebar_ad !!}</textarea>
                                            </div>


                                        </div>


                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Active?</label>
                                        <select class="form-control" name="is_right_sidebar_ad">
                                            <option value="1" {{$ad->right_sidebar_ad ? 'selected' : ''}}>Yes</option>
                                            <option value="0" {{$ad->right_sidebar_ad ? '' : 'selected'}}>No</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="card card-success ">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Bottom Ad</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-lg-7">
                                    <div class="input-group mb-3">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for=""> Code <span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="3" name="bottom_ad"
                                                          id="">{!! $ad->bottom_ad !!}</textarea>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label>Active?</label>
                                        <select class="form-control" name="is_bottom_ad">
                                            <option value="1" {{$ad->is_bottom_ad ? 'selected' : ''}}>Yes</option>
                                            <option value="0" {{$ad->is_bottom_ad ? '' : 'selected'}}>No</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <button type="submit" class="btn btn-block btn-info btn-lg">Записване</button>


            </div>
        </form>

    </div>

@stop


