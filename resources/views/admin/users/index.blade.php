@extends('admin.layouts.app')

@section('title')All Users
@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <h5 class="mb-2">Users</h5>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">All Users</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="">
                                #ID
                            </th>
                            <th style="">
                                Name
                            </th>
                            <th>
                                Registered
                            </th>
                            <th style="">
                                Email
                            </th>

                            <th style="" class="">
                                Role
                            </th>
                           <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($users as $user)
                            <tr>
                                <td>
                                    {{$user->id}}
                                </td>
                                <td>
                                    {{$user->name}}

                                </td>
                                <td>
                                    {{$user->created_at->diffForHumans()}}
                                </td>
                                <td>
                                    {{$user->email}}
                                </td>

                                <td>
                                    {{$user->is_admin ? 'Admin' : 'Regular User'}}
                                </td>
                                <td class="project-actions ">
                                    <a class="btn btn-info btn-sm" href="{{route('admin.users.edit',$user->id)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                        Edit
                                    </a>
                                    <a class="btn btn-danger btn-sm" href="#" data-toggle="modal" data-target="#modal-danger-{{$user->id}}">
                                        <i class="fas fa-trash"></i>
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            <div class="modal fade" id="modal-danger-{{$user->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-danger ">
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title">Delete User Permanently?</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-footer justify-content-between text-center">
                                            <form action="{{route('admin.users.destroy',$user->id)}}" method="POST">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        @empty
                            <hr>
                            <h3 class="ml-4">No Users found</h3>

                        @endforelse


                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </section>
@stop
