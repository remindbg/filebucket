@extends('admin.layouts.app')

@section('title')Edit User {{$user->name}}@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <h5 class="mb-2">Edit User: {{$user->name}}</h5>

            <form action="{{route('admin.users.update',$user->id)}}" method="POST" enctype="multipart/form-data">
                @csrf('put')
                <div class="row">
                    <div class="col-lg-8">
                        @method('put')


                        <div class="card card-info">
                            <div class="card-header">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">General Information</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <p>Name</p>
                                        <div class="input-group mb-3">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text">@</span>
                                            </div>
                                            <input type="text" class="form-control" value="{{$user->name}}"
                                                   name="name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <p>Email Adress</p>
                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                            </div>
                                            <input type="email" class="form-control" value="{{$user->email}}" name="email">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p>UID</p>
                                                <div class="input-group mb-3">

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-user"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" value="{{$user->uid}}" name="uid"
                                                           disabled>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <label>Can User Upload Files?</label>
                                            <select class="form-control" name="is_upload_allowed">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>About</label>
                                            <textarea class="form-control" rows="3" name="about">{{$user->about}}</textarea>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <hr>

                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="card card-blue collapsed-card">
                            <div class="card-header ">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-plus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">Other User Settings</h3>
                            </div>

                        </div>

                    </div>
                </div>
            </form>

        </div>
    </section>
@stop
