@include('admin._static.header')
   @include('admin._static.navbar')

   @include('admin._static.sidebar')

    <div class="content-wrapper">

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>

                </div>
            </div>
        </section>
        <div class="col-lg-12">
            @include('admin.components.messages')
        </div>


        @yield('content')

        <!-- /.content -->

        <a id="back-to-top" href="#" class="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
            <i class="fas fa-chevron-up"></i>
        </a>
    </div>
    <!-- /.content-wrapper -->
@include('admin._static.footer')
