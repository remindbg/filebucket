@extends('admin.layouts.app')

@section('title','General Settings')

@section('content')
    <section class="content">
        <div class="container-fluid">


            <form action="{{route('admin.settings.update',1)}}" method="POST" enctype="multipart/form-data">
                @csrf('put')
                <div class="row">
                    <div class="col-lg-8">
                        @method('put')

                        <div class="card card-info">
                            <div class="card-header">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool"
                                            data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">Theme Settings</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Enable Light/Dark theme switch by front end users</label>
                                            <select class="form-control" name="is_theme_switch_enabled">
                                                <option value="1" {{$settings->is_theme_switch_enabled ?  'selected' : ''}}>Yes</option>
                                                <option value="0" {{$settings->is_theme_switch_enabled ? '' : 'selected'}}>No</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label> Default Theme</label>
                                            <select class="form-control" name="default_theme">
                                                <option value="day" {{$settings->default_theme == 'day' ?  'selected'
                                                 : ''}}>Day</option>
                                                <option value="night"  {{$settings->default_theme == 'night' ?
                                                'selected' : ''}}>Night</option>

                                            </select>
                                        </div>

                                    </div>


                                </div>

                            </div>
                            <hr>
                        </div>
                        <div class="card card-info collapsed-card">
                            <div class="card-header">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">Site Settings</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p>Site Tittle</p>
                                        <div class="input-group mb-3">


                                            <input type="text" class="form-control" value="{{$settings->site_title}}"
                                                   name="site_title">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Site Description</label>
                                            <textarea class="form-control"
                                                      rows="3" name="site_description">  {{$settings->site_description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <p>Site Logo Text</p>
                                        <div class="input-group mb-3">


                                            <input type="text" class="form-control"
                                                   value="{{$settings->site_logo_text}}"
                                                   name="site_logo_text">
                                        </div>
                                    </div>



                                </div>

                            </div>
                        </div>

                        <div class="card card-info collapsed-card">
                            <div class="card-header">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">Custom / Tracking Code</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Custom CSS</label>
                                            <textarea class="form-control" rows="3" name="css_after"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Custom Javascript</label>
                                            <textarea class="form-control" rows="3" name="js_after"></textarea>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-outline-success btn-lg">Save Settings</button>

                    </div>

                    <div class="col-lg-4">
                        <button type="submit" class="btn btn-block btn-outline-success btn-lg">Save Settings</button>
                        <hr>

                        <div class="card card-blue collapsed-card">
                            <div class="card-header ">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-plus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">Disable Current Uploads</h3>
                            </div>
                            <div class="card-body">

                                <div class="form-group">
                                    <label>Disable Uploads?</label>
                                    <select class="form-control" name="disable_current_uploads">
                                        <option value="1" {{$settings->disable_current_uploads ?
                                        'selected' : ''}}>Yes</option>
                                        <option value="0" {{$settings->disable_current_uploads ? '' :
                                        'selected'}}>No</option>
                                    </select>
                                </div>

                            </div>
                        </div>
                        <div class="card card-blue collapsed-card">
                            <div class="card-header ">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-plus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">File Settings</h3>
                            </div>
                            <div class="card-body">

                                <p>General File Settings</p>

                                <hr>
                                <a href="{{route('admin.filesettings.index')}}" class="btn  btn-info btn-block"
                                   target="_blank">Go To
                                    File
                                    Settings</a>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

            <!-- =========================================================== -->
        </div><!-- /.container-fluid -->
    </section>
@stop
