@extends('admin.layouts.app')

@section('title','Directory Cleaner')

@section('content')
    <section class="content">
        <div class="container-fluid">

                <div class="card-body">
                    <form action="{{route('admin.cleaner.remove')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-12">
                                @method('post')

                                <div class="card card-success">
                                    <div class="card-header">
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                    class="fas fa-minus"></i></button>
                                            <button type="button" class="btn btn-tool"
                                                    data-card-widget="remove"><i class="fas fa-times"></i></button>
                                        </div>
                                        <h3 class="card-title">General Information</h3>
                                    </div>
                                    <div class="card-body">

                                        <h5>Total Files in the Directory:  {{count($allfiles)}}</h5>
                                        <h5>Used Files in the Database: {{count($usedfiles)}}</h5>
                                        <hr>
                                        <h5 class="text-center">
                                            @if (count($fordelete) == 1)
                                                {{count($fordelete)}}  File is not used in the database

                                            @elseif (count($fordelete) == 0)
                                                Nothing to Delete
                                            @else
                                                {{count($fordelete)}}  Files are not used in the database
                                            @endif

                                        </h5>
                                    </div>
                                    <hr>
                                    @if (count($fordelete) > 0)
                                        <button type="submit"
                                                class="btn btn-block btn-info btn-lg">
                                            Delete Unused Files
                                        </button>

                                        @else
                                        <a href="#"
                                                class="btn btn-block btn-info btn-lg disabled">
                                            Nothing To Delete
                                        </a>
                                    @endif

                                </div>
                            </div>

                        </div>
                    </form>

                </div>







        </div>
    </section>
@stop
