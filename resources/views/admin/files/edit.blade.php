@extends('admin.layouts.app')

@section('title')Edit File {{$file->saved_name}}@stop

@section('content')
    <section class="content">
        <div class="container-fluid">
            <h5 class="mb-2">Edit File: {{$file->saved_name}}</h5>

            <form action="{{route('admin.files.update',$file->id)}}" method="POST" enctype="multipart/form-data">
                @csrf('put')
                <div class="row">
                    <div class="col-lg-8">
                        @method('put')

                        <div class="card card-info">
                            <div class="card-header">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i></button>
                                    <button type="button" class="btn btn-tool"
                                            data-card-widget="remove"><i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">General Information</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <small>Path of the file.</small>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" value="{{$file->fullpath}}"
                                                   name="fullpath" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">.{{$file->extension}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <p>Original Name</p>
                                        <div class="input-group mb-3">


                                            <input type="text" class="form-control" value="{{$file->original_name}}"
                                                   name="original_name" disabled>
                                            <div class="input-group-prepend">

                                                <span class="input-group-text">{{$file->extension}}</span>
                                            </div>
                                        </div>
                                        <p>Saved Name</p>
                                        <div class="input-group mb-3">


                                            <input type="text" class="form-control" value="{{$file->saved_name}}"
                                                   name="saved_name">
                                            <div class="input-group-prepend">

                                                <span class="input-group-text">{{$file->extension}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <p>File Size</p>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" value="
                                                {{\App\Helpers\Helper::bytesToHuman($file->size)}}"
                                                   name="name" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p>File UID</p>
                                                <div class="input-group mb-3">

                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas
                                                        fa-pager"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" value="{{$file->uid}}"
                                                           name="uid"
                                                           disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Is File Active?</label>
                                            <select class="form-control" name="is_active">
                                                <option value="1" {{$file->is_active ? 'selected' : ''}}>Yes</option>
                                                <option value="0" {{$file->is_active ? '' : 'selected'}}>No</option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <button type="submit" class="btn btn-block btn-info btn-lg">Edit File</button>
                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div class="card card-blue collapsed-card">
                            <div class="card-header ">
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                            class="fas fa-plus"></i></button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i></button>
                                </div>
                                <h3 class="card-title">Views and Downloads</h3>
                            </div>
                            <div class="card-body">
                                <div class="input-group mb-3">

                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas
                                                        fa-eye"></i></span>
                                    </div>
                                    <input type="number" class="form-control" value="{{$file->views}}" name="views"
                                           >
                                </div>
                                <div class="input-group mb-3">

                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fas
                                                        fa-download"></i></span>
                                    </div>
                                    <input type="number" class="form-control" value="{{$file->downloads }}"
                                           name="downloads"
                                           >
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>

        </div>
    </section>
@stop
