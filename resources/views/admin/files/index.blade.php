@extends('admin.layouts.app')

@section('title','All Files')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <h5 class="mb-2">Files</h5>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">All Files</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                            <i class="fas fa-times"></i></button>
                    </div>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th style="">
                                #ID
                            </th>
                            <th style="">
                                MimeType
                            </th>
                            <th style="">
                               Orig.Name
                            </th>
                            <th>Date</th>

                            <th style="">
                               Extension
                            </th>
                            <th>
                                is Active?
                            </th>
                            <th style="" class="">
                               Views
                            </th>
                            <th style="" class="">
                                Downloads
                            </th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse ($files as $file)
                            <tr class="{{$file->is_active ? '' : 'text-danger'}}">
                                <td>
                                    {{$file->id}}
                                </td>
                                <td>
                                    {{$file->mimetype}}

                                </td>
                                <td>{{$file->saved_name}}</td>
                                <td>{{$file->created_at->diffForHumans()}}</td>

                                <td>
                                    .{{$file->extension}}
                                </td>

                                <td>
                                   @if($file->is_active)
                                       Yes
                                    @else
                                        No
                                    @endif

                                </td>
                                <td>
                                    {{$file->views}}
                                </td>
                                <td>
                                    {{$file->downloads}}
                                </td>

                                <td class="project-actions ">
                                    <a class="btn btn-info btn-sm"
                                       href="{{route('files.single',['id' => $file->id,'uid' => $file->uid])}}" target="_blank">
                                        <i class="fas fa-download"></i>
                                        View
                                    </a>

                                    <a class="btn btn-info btn-sm" href="{{route('admin.files.edit',$file->id)}}">
                                        <i class="fas fa-pencil-alt"></i>
                                        Edit
                                    </a>

                                    <a class="btn btn-danger btn-sm" href="#" data-toggle="modal"
                                       data-target="#modal-danger-{{$file->id}}">
                                        <i class="fas fa-trash"></i>
                                        Delete
                                    </a>

                                </td>
                            </tr>
                            <div class="modal fade" id="modal-danger-{{$file->id}}">
                                <div class="modal-dialog">
                                    <div class="modal-content bg-danger ">
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title">Delete File Permanently?</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-footer justify-content-between text-center">
                                            <form action="{{route('admin.files.destroy',$file->id)}}" method="POST">
                                                @method('delete')
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                            <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @empty
                            <hr>
                            <h3 class="ml-4">No Files found</h3>

                        @endforelse


                        </tbody>
                    </table>
                    {{ $files->links() }}
                </div>

            </div>

        </div>
    </section>
@stop
