@extends('layouts.app')

@section('css_after')

@endsection

@section('title','Filebucket -Free and Easy File Uploads')



@section('content')

    <div class="row">

        <div class="col"></div>


        @if ($frontsettings->disable_current_uploads)
            <div class="col-9">
                <div class="row">
                    <div class="col">

                        <p class="mb-4"><a href=""><img src="{{asset('img/digitalocean.png')}}" class="img-fluid"></a></p>
                    </div>
                </div>
                <div class="row"><div class="col"></div><div class="col-8">
                        <p class="lead mt-5 mb-4 text-center">
                          <i class="fa fa-sad-cry fa-fw"></i> Uploading Files is  Currently Disabled
                        </p>
                    </div><div class="col"></div></div>


            </div>

            @else

            <div class="col-9">
                <div class="row">
                    <div class="col">

                        <p class="mb-4"><a href=""><img src="{{asset('img/digitalocean.png')}}" class="img-fluid"></a></p>
                    </div>
                </div>
                <div class="row"><div class="col"></div><div class="col-8">
                        <p class="lead mt-5 mb-4 text-center">
                            Drop your file(s) here or click the button to select them from your drive.
                        </p>
                    </div><div class="col"></div></div>

                <!-- FORM -->
            <!-- <form id="upload-dropzone" class="position-relative mb-5" action="{{route('files.store')}}"> -->
                @csrf

                <div id="actions" class="mb-3">

                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <button class="btn btn-success fileinput-button">
                        <i class="fas fa-plus"></i>
                        <span>Add files...</span>
                    </button>&nbsp;
                    <button type="submit" class="btn btn-primary start d-none">
                        <i class="fas fa-cloud-upload-alt"></i>
                        <span>Upload All</span>
                    </button>&nbsp;
                    <button type="reset" class="btn btn-warning cancel d-none">
                        <i class="fas fa-ban"></i>
                        <span>Cancel</span>
                    </button>

                </div>



                <!-- The global file processing state -->
                <span class="fileupload-process">
                    <div id="total-progress" class="progress progress-striped active" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <div class="progress-bar progress-bar-success" role="progressbar" style="width:0%;" data-dz-uploadprogress></div>
                    </div>
                </span>

                <!-- <div class="dz-message">
                    <p><i class="fas fa-cloud-upload-alt"></i></p>
                    <span>Click or drop files here to upload</span>
                </div> -->


                <div class="table table-striped files mt-3" id="previews">



                </div>


                <div class="file-options mt-4 invisible">

                    <!-- <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" id="allow-commenting" name="" class="custom-control-input">
                        <label class="custom-control-label" for="allow-commenting">Allow commenting</label>
                    </div> -->
                </div>

            </div>
        @endif




            <!-- </div> -->


                <!-- </form> -->
        <div class="col"></div>
    </div>

@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @endsection

@section('scripts')
    <script>




    </script>
@endsection

