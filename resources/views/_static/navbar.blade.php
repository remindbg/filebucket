<nav class="navbar navbar-expand-lg navbar-light bg-transparent pt-3 mb-4">

    <a class="navbar-brand" href="/"><strong>{{$frontsettings->site_logo_text}}</strong></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Left navbar links -->
        <ul class="navbar-nav mr-auto">
            @if ($frontsettings->disable_recent_uploads)

                @else
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('files.latest')}}" class="nav-link">Latest Uploads</a>
                </li>
            @endif

                @if ($frontsettings->disable_popular_uploads)
                  <li class="nav-item d-none d-sm-inline-block">
                      <a href="{{route('files.popular')}}" class="nav-link">Popular Uploads</a>
                  </li>
                    @else
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="{{route('files.latest')}}" class="nav-link">Latest Uploads</a>
                    </li>
                @endif
            @auth()
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{route('admin.dashboard.index')}}" class="nav-link">Admin Panel</a>
                </li>
            @endauth


        </ul>

        <ul class="navbar-nav">

                @if ($frontsettings->is_theme_switch_enabled)
                <li class="nav-item d-none d-sm-inline-block">
                    @if ($frontsettings->is_theme_switch_enabled)
                        @if (Cookie::get('theme')  == 'light')
                            <a href="{{route('theme.switch','night')}}" class="daynight night" title="Night mode"><i
                                    class="fas
                            fa-moon"></i></a>
                            <a href="{{route('theme.switch','day')}}" class="daynight day d-none" title="Day mode"><i
                                    class="fas fa-sun"></i></a>
                        @else
                            <a href="{{route('theme.switch','day')}}" class="daynight day " title="Day mode"><i
                                    class="fas fa-sun"></i></a>

                            <a href="{{route('theme.switch','day')}}" class="daynight night d-none" title="Night
                            mode"><i class="fas fa-moon"></i></a>
                        @endif


                    @endif

                </li>
                @endif

        </ul>


    </div>
</nav>
