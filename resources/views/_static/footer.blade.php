<footer class="main-footer px-3 my-5 pt-5">
    <div class="float-right d-none d-sm-block">
        Version 0.1.0
    </div>
    Copyright &copy; Filebucket - All rights
    reserved.
</footer>

<!-- Control Sidebar -->
{{--<aside class="control-sidebar control-sidebar-dark">--}}
{{--    <!-- Control sidebar content goes here -->--}}
{{--</aside>--}}
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!--  Masonry -->
{{-- <script src="{{asset('js/masonry.min.js')}}"></script> --}}
<!-- jQuery -->
{{-- <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script> --}}
<!-- Bootstrap 4 -->
{{-- <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script> --}}

<script>
    // get dynamic the correct  file upload  route instead of using strings we might forget. It uses the route name
    // from: /routes/web.php
    const uploadRoute = '{{route('files.store')}}'; 
</script>

<script src="{{asset('js/app.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('js/demo.js')}}"></script>



{{-- <script src="{{asset('js/dropzone.js')}}"></script> --}}

@yield('scripts')

{{-- <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script> --}}

</body>
</html>
