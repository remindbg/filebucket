<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





/*
 * Unprotected Routes
 */

Route::get('/', 'HomeController@index')->name('home.index');
Route::get('/latestuploads', 'FileController@latest')->name('files.latest');
Route::get('/popularuploads', 'FileController@popular')->name('files.popular');

Route::get('/test', 'HomeController@testmessages')->name('home.testmessages');
Route::get('/download/{id}/{_uid}', 'FileController@single')->name('files.single');
Route::get('/theme/{theme}', 'ThemeController@switchTheme')->name('theme.switch');


Route::post('/files/upload', 'UploadController@store')->name('files.store');
Route::post('/files/download/{id}/{uid}', 'DownloadController@single')->name('download.single');

Route::get('/files/staticupload', 'FileController@staticuploadget')->name('files.staticuploadget');
Route::post('/files/staticupload', 'FileController@staticuploadpost')->name('files.staticuploadpost');
Route::post('files/settingsmeta', 'FileController@getMetaFileSettings')->name('files.checksettingssize');

/*
 * Protected Routes for front end / auth only
 */
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Auth::routes();


/*
 * Start wrapping the Admin Middleware protected Routes
 */

Route::namespace('Admin')->prefix('rpanel')->name('admin.')->middleware('isAdmin')->group(function () {

    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    Route::resource('users','UserController');
    Route::resource('files','FileController');
    Route::resource('settings','SettingsController');
    Route::resource('comments','CommentController');
    Route::resource('ads','AdController');
    Route::resource('reports','ReportController');
    Route::resource('filesettings','FileSettingsController');
    Route::resource('staticpages','StaticPageController');
    Route::resource('siteinfo','SiteInfoController');
    Route::resource('cleaner','CleanerController');
    Route::resource('ads','AdController');

    Route::post('settings/dircleaner/remove', 'CleanerController@remove')->name('cleaner.remove');

});
